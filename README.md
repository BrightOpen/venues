# Venues

The idea is to provide a privacy friendly solution to finding peers online, that means without giving out too much information to anyone, especially the service provider.

The package contains a server bin and a client lib.