use std::time::{Duration, Instant};

use mainline::{hash_immutable, Dht, Id};

fn main() -> anyhow::Result<()> {
    env_logger::init();

    let dht = Dht::builder()
        .as_server()
        //.bootstrap(&[])
        .port(45678)
        .build();

    let infohash = mainline::hash_immutable(b"There must be an end to it!").into();
    // println!("\nLooking up peers for infohash: {} ...\n", infohash);
    // let res = dht.put_immutable("hi there!".into())?;
    // println!("hello {:?}", res.stored_at());
    // println!("hello {}", res.target());
    // let infohash = res.target();

    let res = dht.announce_peer(infohash, None)?;

    let stored_at = res.stored_at();
    println!("Stored at: {:?} nodes", stored_at.len());
    for node in stored_at {
        println!("   {:?}", node);
    }

    loop {
        let start = Instant::now();
        let mut first = false;

        let mut count = 0;

        let mut response = dht.get_peers(infohash);

        for res in &mut response {
            if !first {
                first = true;
                println!(
                    "Got first result in {:?} seconds\n",
                    start.elapsed().as_secs_f32()
                );

                println!("Streaming peers:\n");
            }

            count += 1;
            println!("Got peer {:?} | from: {:?}", res.peer, res.from);
        }

        println!(
            "Visited {:?} nodes, found {:?} closest nodes",
            response.visited,
            &response.closest_nodes.len()
        );

        println!(
            "\nQuery exhausted in {:?} seconds, got {:?} peers.",
            start.elapsed().as_secs_f32(),
            count
        );

        std::thread::sleep(Duration::from_secs(2));
    }

    Ok(())
}
