use std::time::Duration;
use venues::{Client, Response, VenueId};

#[async_std::main]
async fn main() {
    let venue = VenueId::new("test client");
    let mut client1 = Client::new("168.119.63.232:443", "0.0.0.0:0");

    let mut client2 = Client::new("168.119.63.232:443", "0.0.0.0:0");
    let mut chat = true;
    for _ in 0..5000 {
        if chat {
            for i in 0..10 {
                client1.chat(&venue, format!("I'm finally here... {i}"));
                client2.chat(&venue, format!("hello everyone! {i}"));
            }
        } else {
            client1.chat(&venue, "");
            client2.chat(&venue, "");
        }
        let c1 = responses(&mut client1, chat).await;
        let c2 = responses(&mut client2, chat).await;
        chat = c1 || c2;
    }
    responses(&mut client1, true).await;
    responses(&mut client2, true).await;
    client1.gone(None);
    client2.gone(Some(&venue));
    responses(&mut client1, false).await;
    responses(&mut client2, false).await;
}

async fn responses<T, B: std::fmt::Display>(client: &mut Client<T, B>, chat: bool) -> bool {
    let bound = client
        .bound
        .map(|b| b.to_string())
        .unwrap_or_else(|| client.bind.to_string());
    
        
    let mut got = 0;
    let timeout = if chat {
        Duration::from_millis(1000)
    } else {
        Duration::from_secs(10)
    };
    for Response { message, from } in client.response(timeout).await.expect("responses") {
        got += 1;
        match message {
            venues::Message::Cht(_,_,_) => {}
            _ => eprintln!("c {bound} from {from}: {message:?}"),
        }
    }

    eprintln!("c {bound} {got} responses");
    got > 1
}
