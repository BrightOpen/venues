use std::{
    collections::BTreeSet,
    net::SocketAddr,
    time::{Duration, Instant},
};

use anyhow::Context;
use log::*;
use mainline::{async_dht::AsyncDht, Dht, Id};

#[async_std::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();

    let dht = Dht::builder()
        //.bootstrap(&["localhost:45678".into()])
        .build()
        .as_async();

    let local_addr = dht.local_addr().await?;
    println!("My local address: {}", local_addr);

    let my_addrs = get_my_addrs(&dht).await.context("get_my_addrs")?;
    println!("My public addresses: {:?}", my_addrs);

    let infohash = mainline::hash_immutable(b"There must be an end to it!").into();
    let mut res = dht.announce_peer(infohash, None).await?;

    // let res = dht.put_immutable("hello!".into())?;
    // println!("hello {:?}", res.stored_at());
    // println!("hello {}", res.target());
    //let infohash = res.target();

    // let res = dht.announce_peer(infohash, Some(22334))?;
    // let stored_at = res.stored_at();
    // println!("Stored at: {:?} nodes", stored_at.len());
    // for node in stored_at {
    //     println!("   {:?}", node);
    // }

    loop {
        let start = Instant::now();
        let mut peers = BTreeSet::new();
        let mut response = dht.get_peers(infohash);
        for res in &mut response {
            trace!("Got {:?} | from: {:?}", res.peer, res.from);
            peers.insert(res.peer);
        }

        println!("Unique peers: {peers:?}");
        let peers = peers.difference(&my_addrs).collect::<Vec<_>>();
        println!("Other peers: {peers:?}");

        println!(
            "Visited {:?} nodes, found {:?} closest nodes",
            response.visited,
            &response.closest_nodes.len()
        );

        println!(
            "Query exhausted in {:?} seconds",
            start.elapsed().as_secs_f32()
        );
        std::thread::sleep(Duration::from_secs(20));
    }

    Ok(())
}

/// Find my own addresses by announcing myself to random infohash
async fn get_my_addrs(dht: &AsyncDht) -> Result<BTreeSet<SocketAddr>, mainline::Error> {
    for _ in 0..3 {
        let mut addrs = vec![];
        // in case we hit a used hash, try a few times
        for _ in 0..3 {
            let infohash = Id::random();
            if terminal(dht.announce_peer(infohash, None).await)?
                .map_err(|e| warn!("get_my_addr - Announce error {e}"))
                .is_err()
            {
                continue;
            }
            addrs.push(
                dht.get_peers(infohash)
                    .into_iter()
                    .map(|res| {
                        trace!("get_my_addrs {:?} | from: {:?}", res.peer, res.from);
                        res.peer
                    })
                    .collect::<BTreeSet<_>>(),
            );
        }
        addrs.dedup();
        if addrs.len() == 1 {
            return Ok(addrs.pop().expect("single"));
        }
    }
    Ok(BTreeSet::new())
}

fn terminal<T>(v: mainline::Result<T>) -> Result<Result<T, mainline::Error>, mainline::Error> {
    match v {
        Ok(v) => Ok(Ok(v)),
        Err(e) => match &e {
            mainline::Error::Generic(_) => Err(e),
            mainline::Error::Static(_) => Err(e),
            mainline::Error::IO(_) => Ok(Err(e)),
            mainline::Error::InvalidIdSize(_) => Err(e),
            mainline::Error::InvalidIdEncoding(_) => Err(e),
            mainline::Error::BencodeError(_) => Ok(Err(e)),
            mainline::Error::InvalidTransactionId(_) => Ok(Err(e)),
            mainline::Error::Receive(_) => Err(e),
            mainline::Error::InvalidMutableSignature => Ok(Err(e)),
            mainline::Error::InvalidMutablePublicKey => Ok(Err(e)),
        },
    }
}
