use std::{
    pin::Pin,
    task::{Context, Poll},
};

use async_std::channel::{unbounded, Receiver, Sender};
use futures::{stream::FuturesUnordered, Future, Stream};

/// accept futures and await them all
pub struct Worx<T> {
    futures: FuturesUnordered<T>,
    future_rx: Receiver<T>,
    future_tx: Sender<T>,
}

impl<T> Worx<T> {
    pub fn new() -> Self {
        let (future_tx, future_rx) = unbounded();
        let futures = Default::default();
        Self {
            futures,
            future_rx,
            future_tx,
        }
    }
    pub fn tx(&self) -> &Sender<T> {
        &self.future_tx
    }
}
impl<T> Future for Worx<T>
where
    T: Future,
{
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        loop {
            break match Pin::new(&mut self.future_rx).poll_next(cx) {
                Poll::Ready(Some(future)) => {
                    self.futures.push(future);
                    continue;
                }
                Poll::Ready(None) if self.futures.is_empty() => Poll::Ready(()),
                Poll::Pending if self.futures.is_empty() => Poll::Pending,
                Poll::Ready(None) | Poll::Pending => {
                    match Pin::new(&mut self.futures).poll_next(cx) {
                        Poll::Ready(Some(_)) => continue,
                        Poll::Ready(None) => continue,
                        Poll::Pending => Poll::Pending,
                    }
                }
            };
        }
    }
}
