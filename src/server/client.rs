use std::{net::SocketAddr, sync::Arc};

use async_std::{channel::Receiver, net::UdpSocket, stream::StreamExt};
use futures::stream::FuturesOrdered;
use log::*;
use webrtc_dtls::{config::Config, conn::DTLSConn};

use crate::session::RecvUdpConn;

pub async fn setup(
    local_address: SocketAddr,
    udp_sock: Arc<UdpSocket>,
    udp_rx: Receiver<Vec<u8>>,
    addr: String,
    ca_cert: String,
) -> anyhow::Result<Arc<DTLSConn>> {
    let (addr, server_name) = match addr.as_str().rsplit_once(":") {
        Some((name, _port)) => (addr.to_owned(), name.into()),
        None => (format!("{addr}:443"), addr),
    };

    let addrs = async_std::net::ToSocketAddrs::to_socket_addrs(&addr)
        .await?
        .collect::<Vec<_>>();

    debug!("addr {addr} resolved to {addrs:?}");
    debug!("we've got  {local_address:?}");

    let mut config = Config {
        server_name,
        ..Default::default()
    };

    if ca_cert == "insecure" {
        warn!("Skipping CA cert verification");
        config.insecure_skip_verify = true;
    } else if ca_cert.is_empty() {
        info!("No specific CA cert used, will default to system store");
    } else {
        info!("Using CA Cert {ca_cert:?}");
        let cert = rustls_pemfile::certs(&mut ca_cert.as_bytes())
            .next()
            .expect("cert")
            .expect("cert ok");
        let (added, _ignored) = config.roots_cas.add_parsable_certificates(&[cert]);
        assert_eq!(added, 1);
    }

    let conn = addrs
        .into_iter()
        .filter_map(|remote_address| match (local_address, remote_address) {
            (SocketAddr::V4(_), SocketAddr::V4(_)) | (SocketAddr::V6(_), SocketAddr::V6(_)) => {
                Some(remote_address)
            }
            (SocketAddr::V4(_), SocketAddr::V6(_)) | (SocketAddr::V6(_), SocketAddr::V4(_)) => None,
        })
        .map(move |remote_address| {
            let config = config.clone();
            let udp_rx = udp_rx.clone();
            let udp_sock = udp_sock.clone();
            async move {
                let conn = RecvUdpConn::new(udp_rx, udp_sock, remote_address, local_address);
                info!("Trying {} -> {}", local_address, remote_address);
                let dtls_conn = Arc::new(DTLSConn::new(Arc::new(conn), config, true, None).await?);
                anyhow::Ok(dtls_conn)
            }
        })
        .collect::<FuturesOrdered<_>>()
        .filter_map(|conn| match conn {
            Ok(conn) => Some(conn),
            Err(e) => {
                warn!("Connection failed {e}");
                None
            }
        })
        .next()
        .await
        .ok_or_else(|| anyhow::anyhow!("Could not connect to {addr}"))?;

    Ok(conn)
}
