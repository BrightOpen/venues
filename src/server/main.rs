mod acme;
mod reps;
mod session;
mod talk;
mod works;
mod client;

use std::pin::Pin;

use async_std::channel::unbounded;
use futures::{select, Future, FutureExt};
use log::*;

#[async_std::main]
async fn main() {
    env_logger::init();

    let bind = std::env::var("LISTEN_ON").unwrap_or("localhost:0".to_owned());
    let domains = std::env::var("DOMAIN")
        .unwrap_or_else(|_| {
            bind.rsplit_once(":")
                .map(|(host, _port)| host.to_owned())
                .unwrap_or(bind.clone())
        })
        .split(",")
        .map(|s| s.to_owned())
        .collect::<Vec<_>>();
    let contact = std::env::var("CONTACT").ok();
    let prod = std::env::var("ENV") == Ok("production".to_owned());
    info!("Binding {bind} for domains {domains:?}, ACME contact {contact:?}, prod: {prod}");

    let worx = works::Worx::new();
    let worx_tx = worx.tx().clone();
    let mut worx = worx.fuse();

    {
        let (ban_tx, ban_rx) = unbounded();
        let (rep_tx, rep_rx) = unbounded();
        let mut reps = Box::pin(reps::reps(rep_rx, ban_tx)).fuse();

        let (cert_tx, cert_rx) = unbounded();
        worx_tx
            .send(Box::pin(acme::acme(
                prod,
                bind.clone(),
                domains.clone(),
                contact.clone(),
                cert_tx.clone(),
                None, // TODO: webserver
            )) as Pin<Box<dyn Future<Output = ()>>>)
            .await
            .expect("acme scheduling");

        loop {
            let mut talk = Box::pin(talk::talk(
                bind.clone(),
                worx_tx.clone(),
                rep_tx.clone(),
                ban_rx.clone(),
                talk::new_certs(domains.clone(), cert_rx.clone()),
            ))
            .fuse();

            select! {
                () = talk => break warn!("talk finished"),
                () = reps => break error!("reps broke"),
                () = worx => break error!("worx broke")
            }
        }
    }
    info!("Shutdown, gracefully await pending worx");
    worx.await
}
